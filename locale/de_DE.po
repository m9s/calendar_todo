# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:calendar.todo:0"
msgid "Recurrence can not be recurrent!"
msgstr "Wiederholungen können nicht rekursiv sein!"

msgctxt "error:calendar.todo:0"
msgid "UUID and recurrence must be unique in a calendar!"
msgstr ""
"UUID und Wiederholung können in einem Kalender nicht mehrfach vergeben "
"werden!"

msgctxt "field:calendar.todo,alarms:0"
msgid "Alarms"
msgstr "Alarm"

msgctxt "field:calendar.todo,attendees:0"
msgid "Attendees"
msgstr "Teilnehmer"

msgctxt "field:calendar.todo,calendar:0"
msgid "Calendar"
msgstr "Kalender"

msgctxt "field:calendar.todo,calendar_owner:0"
msgid "Owner"
msgstr "Besitzer"

msgctxt "field:calendar.todo,calendar_read_users:0"
msgid "Read Users"
msgstr "Benutzer mit Leseberechtigung"

msgctxt "field:calendar.todo,calendar_write_users:0"
msgid "Write Users"
msgstr "Benutzer mit Schreibberechtigung"

msgctxt "field:calendar.todo,categories:0"
msgid "Categories"
msgstr "Kategorien"

msgctxt "field:calendar.todo,classification:0"
msgid "Classification"
msgstr "Klassifizierung"

msgctxt "field:calendar.todo,completed:0"
msgid "Completed"
msgstr "Abgeschlossen"

msgctxt "field:calendar.todo,description:0"
msgid "Description"
msgstr "Bezeichnung"

msgctxt "field:calendar.todo,dtstart:0"
msgid "Start Date"
msgstr "Anfangsdatum"

msgctxt "field:calendar.todo,due:0"
msgid "Due Date"
msgstr "Fälligkeitsdatum"

msgctxt "field:calendar.todo,exdates:0"
msgid "Exception Dates"
msgstr "Ausnahmedaten"

msgctxt "field:calendar.todo,exrules:0"
msgid "Exception Rules"
msgstr "Ausnahmeregeln"

msgctxt "field:calendar.todo,location:0"
msgid "Location"
msgstr "Ort"

msgctxt "field:calendar.todo,occurences:0"
msgid "Occurences"
msgstr "Ereignisse"

msgctxt "field:calendar.todo,organizer:0"
msgid "Organizer"
msgstr "Organisator"

msgctxt "field:calendar.todo,parent:0"
msgid "Parent"
msgstr "Übergeordnet (Aufgabe)"

msgctxt "field:calendar.todo,percent_complete:0"
msgid "Percent complete"
msgstr "% erledigt"

msgctxt "field:calendar.todo,rdates:0"
msgid "Recurrence Dates"
msgstr "Wiederholungsdaten"

msgctxt "field:calendar.todo,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo,recurrence:0"
msgid "Recurrence"
msgstr "Wiederholung"

msgctxt "field:calendar.todo,rrules:0"
msgid "Recurrence Rules"
msgstr "Wiederholungsregeln"

msgctxt "field:calendar.todo,sequence:0"
msgid "Sequence"
msgstr "Revision"

msgctxt "field:calendar.todo,status:0"
msgid "Status"
msgstr "Status"

msgctxt "field:calendar.todo,summary:0"
msgid "Summary"
msgstr "Zusammenfassung"

msgctxt "field:calendar.todo,timezone:0"
msgid "Timezone"
msgstr "Zeitzone"

msgctxt "field:calendar.todo,uuid:0"
msgid "UUID"
msgstr "UUID"

msgctxt "field:calendar.todo,vtodo:0"
msgid "vtodo"
msgstr "vtodo"

msgctxt "field:calendar.todo-calendar.category,category:0"
msgid "Category"
msgstr "Kategorie"

msgctxt "field:calendar.todo-calendar.category,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo-calendar.category,todo:0"
msgid "To-Do"
msgstr "Aufgabe"

msgctxt "field:calendar.todo.alarm,calendar_alarm:0"
msgid "Calendar Alarm"
msgstr "Kalender Alarm"

msgctxt "field:calendar.todo.alarm,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo.alarm,todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "field:calendar.todo.attendee,calendar_attendee:0"
msgid "Calendar Attendee"
msgstr "Kalender Teilnehmer"

msgctxt "field:calendar.todo.attendee,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo.attendee,todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "field:calendar.todo.exdate,calendar_date:0"
msgid "Calendar Date"
msgstr "Kalender Datum"

msgctxt "field:calendar.todo.exdate,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo.exdate,todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "field:calendar.todo.exrule,calendar_rrule:0"
msgid "Calendar RRule"
msgstr "Kalender WRegel"

msgctxt "field:calendar.todo.exrule,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo.exrule,todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "field:calendar.todo.rdate,calendar_date:0"
msgid "Calendar Date"
msgstr "Kalender Datum"

msgctxt "field:calendar.todo.rdate,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo.rdate,todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "field:calendar.todo.rrule,calendar_rrule:0"
msgid "Calendar RRule"
msgstr "Kalender WRegel"

msgctxt "field:calendar.todo.rrule,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:calendar.todo.rrule,todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "help:calendar.todo,uuid:0"
msgid "Universally Unique Identifier"
msgstr "Universally Unique Identifier"

msgctxt "model:calendar.todo,name:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "model:calendar.todo-calendar.category,name:0"
msgid "Todo - Category"
msgstr "Aufgabe - Kategorie"

msgctxt "model:calendar.todo.alarm,name:0"
msgid "Alarm"
msgstr "Alarm"

msgctxt "model:calendar.todo.attendee,name:0"
msgid "Attendee"
msgstr "Teilnehmer"

msgctxt "model:calendar.todo.exdate,name:0"
msgid "Exception Date"
msgstr "Ausnahmedatum"

msgctxt "model:calendar.todo.exrule,name:0"
msgid "Exception Rule"
msgstr "Ausnahmeregel"

msgctxt "model:calendar.todo.rdate,name:0"
msgid "Todo Recurrence Date"
msgstr "Aufgabe Wiederholungsdatum"

msgctxt "model:calendar.todo.rrule,name:0"
msgid "Recurrence Rule"
msgstr "Wiederholungsregel"

msgctxt "model:ir.action,name:act_todo_form"
msgid "Todos"
msgstr "Aufgaben"

msgctxt "model:ir.action,name:act_todo_form3"
msgid "Todos"
msgstr "Aufgaben"

msgctxt "model:ir.ui.menu,name:menu_todo_form"
msgid "Todos"
msgstr "Aufgaben"

msgctxt "selection:calendar.todo,classification:0"
msgid "Confidential"
msgstr "Vertraulich"

msgctxt "selection:calendar.todo,classification:0"
msgid "Private"
msgstr "Privat"

msgctxt "selection:calendar.todo,classification:0"
msgid "Public"
msgstr "Öffentlich"

msgctxt "selection:calendar.todo,status:0"
msgid ""
msgstr ""

msgctxt "selection:calendar.todo,status:0"
msgid "Cancelled"
msgstr "Abgesagt"

msgctxt "selection:calendar.todo,status:0"
msgid "Completed"
msgstr "Abgeschlossen"

msgctxt "selection:calendar.todo,status:0"
msgid "In-Process"
msgstr "In Arbeit"

msgctxt "selection:calendar.todo,status:0"
msgid "Needs-Action"
msgstr "Benötigt Eingriff"

msgctxt "view:calendar.todo.attendee:0"
msgid "Attendee"
msgstr "Teilnehmer"

msgctxt "view:calendar.todo.attendee:0"
msgid "Attendees"
msgstr "Teilnehmer"

msgctxt "view:calendar.todo.exdate:0"
msgid "Exception Date"
msgstr "Ausnahmedatum"

msgctxt "view:calendar.todo.exdate:0"
msgid "Exception Dates"
msgstr "Ausnahmedaten"

msgctxt "view:calendar.todo.exrule:0"
msgid "Exception Rule"
msgstr "Ausnahmeregel"

msgctxt "view:calendar.todo.exrule:0"
msgid "Exception Rules"
msgstr "Ausnahmeregeln"

msgctxt "view:calendar.todo.rdate:0"
msgid "Recurrence Date"
msgstr "Wiederholungsdatum"

msgctxt "view:calendar.todo.rdate:0"
msgid "Recurrence Dates"
msgstr "Wiederholungsdaten"

msgctxt "view:calendar.todo.rrule:0"
msgid "Recurrence Rule"
msgstr "Wiederholungsregel"

msgctxt "view:calendar.todo.rrule:0"
msgid "Recurrence Rules"
msgstr "Wiederholungsregeln"

msgctxt "view:calendar.todo:0"
msgid "Attendees"
msgstr "Teilnehmer"

msgctxt "view:calendar.todo:0"
msgid "Categories"
msgstr "Kategorien"

msgctxt "view:calendar.todo:0"
msgid "General"
msgstr "Allgemein"

msgctxt "view:calendar.todo:0"
msgid "Occurences"
msgstr "Ereignisse"

msgctxt "view:calendar.todo:0"
msgid "Recurrences"
msgstr "Wiederholungen"

msgctxt "view:calendar.todo:0"
msgid "Todo"
msgstr "Aufgabe"

msgctxt "view:calendar.todo:0"
msgid "Todos"
msgstr "Aufgaben"
